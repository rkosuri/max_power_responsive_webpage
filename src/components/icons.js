function Icons() {
  var shortcuts = [
    { icon: "fa fa-html5", id: "html", name: "HTML" },
    { icon: "fa fa-css3", id: "css", name: "CSS" },
    { icon: "fa fa-search", id: "seo", name: "SEO" },
    { icon: "fa fa-users", id: "social", name: "Social" },
  ];
  return (
    <>
      {shortcuts.map((data) => {
        return <Col id={data.id} icon={data.icon} name={data.name} />;
      })}
    </>
  );
}

function Col(props) {
  return (
    <>
      <section className="one-fourth" id={props.id}>
        <td>
          <i className={props.icon}></i>
        </td>

        <h3>{props.name}</h3>
      </section>
    </>
  );
}

export default Icons;
